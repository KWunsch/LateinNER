import spacy
import re

nlp = spacy.load("la_core_web_lg")

with open('test.txt', 'r') as file:
    text = file.read()

doc = nlp(text)

with open("result.txt", 'w') as file:
    i = 0
    for ent in doc.ents:
        file.write(f"{i}: "+ ent.text + " " + ent.label_ + "\n")
        i +=1











# for ent in doc.ents:
#     entity_type = ent.label_
#     entity_text = ent.text
#     replacement = f"<rs type=\"{entity_type.lower()}\">{entity_text}</rs>"
#     text = re.sub(r'\b' + entity_text + r'\b', replacement, text)
#
# print(text)