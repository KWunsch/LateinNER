<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="3.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:tei="http://www.tei-c.org/ns/1.0">
    
    <!-- Process the document and apply templates only to the tei:text and tei:body elements -->
    
    <!-- Match the root element and apply templates to its children -->
    <xsl:template match="/tei:TEI">
        <xsl:apply-templates select="tei:text/tei:body"/>
    </xsl:template>
    
    <xsl:template match="tei:pc | tei:lb | tei:note | tei:bibl | tei:seg" ></xsl:template>
    
    <xsl:template match="tei:w">
        <xsl:apply-templates></xsl:apply-templates>
    </xsl:template>
    
</xsl:stylesheet>

