# Latein-NER 
Repo mit den ersten Versuchen zur named entity recognition auf lateinische Texte aus den Europäischen Religionsfrieden Digital

# verwendete Bibliotheken
spacy, re

# verwendetes Model
https://huggingface.co/latincy/la_core_web_lg


# Beispiel-Text
https://exist.ulb.tu-darmstadt.de/2/v/pa000008-0109

rudimentäre Transformation zu text-file, siehe txt.xsl

# Ergebnisse
in result.txt findet sich eine Liste der Ergebnisse